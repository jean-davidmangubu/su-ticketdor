<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MailManager
{
    private $mailer;
    private $templating;
    private $parameters;

    public function __construct( \Swift_Mailer $mailer, \Twig_Environment $templating, ParameterBagInterface $parameters )
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    public function sendSuspectWeeklyGainsAmountEmail( $user )
    {
        $mail = (new \Swift_Message( '[Tickets d\'Or] Un utilisateur a dépassé le seuil de gains hebdomadaire' ) )
            ->setFrom( $this->parameters->get( 'app.admin_email' ) )
            ->setTo( ['alexandre.dacosta@systeme-u.fr', 'marion.chantry@5emegauche.com', 'jean-david.mangubu@5emegauche.com'])
            ->setBody( $this->templating->render(
                'mail/suspect-weekly-gains-amount.html.twig',
                [ 'user' => $user ]
            ), 'text/html' )
        ;
        return $this->mailer->send( $mail );
    }

    public function sendSuspectDailyGainsAmountEmail( $user )
    {
        $mail = (new \Swift_Message( '[Tickets d\'Or] Un utilisateur a dépassé le seuil de gains quotidien' ) )
            ->setFrom( $this->parameters->get( 'app.admin_email' ) )
            ->setTo( ['alexandre.dacosta@systeme-u.fr', 'marion.chantry@5emegauche.com', 'jean-david.mangubu@5emegauche.com'])
            ->setBody( $this->templating->render(
                'mail/suspect-daily-gains-amount.html.twig',
                [ 'user' => $user ]
            ), 'text/html' )
        ;
        return $this->mailer->send( $mail );
    }

    public function sendGameSessionCreationConfirmation( $user )
    {
        $mail = (new \Swift_Message( '[Tickets d\'Or] Vous avez entré un nouveau code' ) )
            ->setFrom( $this->parameters->get( 'app.admin_email' ) )
            ->setTo( $user->getEmail() )
            ->setBody( $this->templating->render(
                'mail/game-session-creation.html.twig',
                [
                    'user' => $user,
                    'app_url' => $this->parameters->get( 'app.url' ),
                    'faq_url' => $this->parameters->get( 'app.faq_url' )
                ]
            ), 'text/html' )
        ;
        return $this->mailer->send( $mail );
    }

    public function sendGameSessionAttemptSuccess( $user, $rewardAmount )
    {
        //$mail = (new \Swift_Message( '[Tickets d\'Or] Vous avez remporté ' . $rewardAmount . '€ sur votre Carte U' ) )
        $mail = (new \Swift_Message( $user->getFirstname().', toutes nos félicitations...' ) )
            ->setFrom( ['nepasrepondre@jeu.magasins-u.com' => 'Votre Magasin U'] )
            ->setTo( $user->getEmail() )
            ->setBody( $this->templating->render(
                'mail/game-session-attempt-success.html.twig',
                [
                    'user' => $user,
                    'app_url' => 'https://www.magasins-u.com/leticketdor',
                    'faq_url' => $this->parameters->get( 'app.faq_url' ),
                    'reward_amount' => $rewardAmount,
                    'total_reward' => $user->getTotalGainsAmount() + $rewardAmount
                ]
            ), 'text/html' )
        ;

        return $this->mailer->send( $mail );
    }
}