<?php

namespace App\Command;

use App\Entity\InstantWin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GenerateInstantWinsCommand extends Command
{
    protected static $defaultName = 'app:generate-instant-wins';
    private $em;
    private $parameters;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameters)
    {
        $this->em = $em;
        $this->parameters = $parameters;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Generates instant wins according to the configuration file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $instantWins = $this->parameters->get( 'app.instant_wins' );

        // On désactive les logs de Doctrine pour éviter d'atteindre le memory_limit
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        foreach ( $instantWins as $day )
        {
            $totalDuration = $day[ 'end_date' ] - $day[ 'start_date' ];
            $rewardsNumber = $this->countRewardsNumber( $day );
            $iwDuration = floor( $totalDuration / $rewardsNumber );

            for ( $i = 0; $i < $rewardsNumber; $i++ )
            {
                $day['stacked_reward'][] = $this->getRandomReward( $day,  new \DateTime( '@' . ( ( $iwDuration * $i ) + $day[ 'start_date' ] ) ) ) ;
            }
            shuffle($day['stacked_reward']);
            for ( $i = 0; $i < $rewardsNumber; $i++ )
            {
                $iw = new InstantWin();
                $iw->setStartDate( new \DateTime( '@' . ( ( $iwDuration * $i ) + $day[ 'start_date' ] ) ) );
                $iw->setEndDate( new \DateTime( '@' . ( 
                    // S'il s'agit de la dernière dotation de la journée
                    // on lui met la date de fin de journée en date de fin
                    $i + 1 < $rewardsNumber
                    ? ( ( $iwDuration * ( $i + 1 ) ) + $day[ 'start_date' ] )
                    : $day[ 'end_date' ]
                ) ) );
                //$iw->setRewardAmount( $this->getRandomReward( $day, $iw->getStartDate() ) );
                $iw->setRewardAmount( $day['stacked_reward'][$i] );
                $this->em->persist( $iw );
                if ( $i % 100 == 0 )
                {
                    $this->em->flush();
                }
            }
            $this->em->flush();
            $this->em->clear();

            $io->note( sprintf(
                '%d instant wins generated from %s to %s',
                $rewardsNumber,
                (new \DateTime( '@' . $day[ 'start_date' ] ))->format( 'Y-m-d H:i:s' ),
                (new \DateTime( '@' . $day[ 'end_date' ] ))->format( 'Y-m-d H:i:s' )
            ) );
        }

        $io->success('Instant wins has successfully been generated.');
    }

    /**
     * Permet de compter le nombre total de dotation dans la journée
     */
    protected function countRewardsNumber( $day )
    {
       return array_reduce( $day[ 'rewards' ], function( $acc, $reward ) {
            return $acc + $reward[ 'quantity' ];
       }, 0 ); 
    }

    /**
     * Récupère une dotation au hasard parmis toutes celles disponibles
     */
    protected function getRandomReward( &$day, $time )
    {
        $availableIndexes = $this->getAvailableRewardsIndexes( $time, $day[ 'rewards' ] );

        $rewardIndex = $availableIndexes[ random_int( 0, count( $availableIndexes ) - 1 ) ];
        $amount = $day[ 'rewards' ][ $rewardIndex ][ 'amount' ];
        $day[ 'rewards' ][ $rewardIndex ][ 'quantity' ]--;
        if (  $day[ 'rewards' ][ $rewardIndex ][ 'quantity' ] == 0 )
        {
            unset( $day[ 'rewards' ][ $rewardIndex ] );
        }

        return $amount;
    }

    /**
     * Permet de récupérer les index des dotations disponibles à cette période de la journée car
     * certaines disponibles ne sont disponibles que le soir
     */
    protected function getAvailableRewardsIndexes( $time, $rewards )
    {
        return array_reduce( array_keys( $rewards ), function( $acc, $index ) use ( $rewards, $time ) {
            $startDate = !empty( $rewards[ $index ][ 'start_date' ] ) ? new \DateTime( '@' . $rewards[ $index ][ 'start_date' ] ) : null;
            if ( !$startDate || $startDate < $time )
            {
                $acc[] = $index;
            }
            return $acc;
        }, [] );
    }
}
