<?php

namespace App\Command;

use App\Entity\Code;
use App\Entity\InstantWin;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportCodesCommand extends Command
{
    protected static $defaultName = 'app:import-codes';
    private $doctrine;

    public function __construct( ManagerRegistry $doctrine )
    {
        $this->doctrine = $doctrine;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Import codes from files specified in the arguments')
            ->addArgument(
                'files',
                InputArgument::IS_ARRAY,
                'Files containing codes to import'
            );
        ;
    }

    protected function execute( InputInterface $input, OutputInterface $output )
    {
        $io = new SymfonyStyle($input, $output);

        // On désactive les logs de Doctrine pour éviter d'atteindre le memory_limit
        $this->getManager()->getConnection()->getConfiguration()->setSQLLogger( null );

        $files = $input->getArgument( 'files' );
        foreach ( $files as $fileName )
        {
            if ( file_exists( $fileName ) )
            {
                $i = 0;
                $file = fopen( $fileName, 'r' );
                while ( ( $line = fgets( $file ) ) !== false )
                {
                    $line = substr( $line, 0, 8 );
                    if ( strlen( $line ) == 8 )
                    {
                        $code = new Code( $line );
                        $this->getManager()->persist( $code );
                        if ( $i % 1000 == 0 )
                        {
                            $this->flush( $io );
                        }
                        $i++;
                    }
                }
                $this->flush( $io );
                $io->text( $fileName . ': Successfully imported.' );
            }
            else
            {
                $io->error( $fileName . ': No such file or directory.' );
            }
        }
        $io->success( 'All codes have been successfully imported.' );
    }

    public function flush( $io )
    {
        try {
            $this->getManager()->flush();
            $this->getManager()->clear();
        }
        catch( UniqueConstraintViolationException $e )
        {
            preg_match( '/Duplicate entry \'([A-Z0-9]{8})\'/', $e->getMessage(), $matches );
            $io->error( 'Duplicate code: \'' . $matches[ 1 ] . '\'' );
            die();
        }
    }

    public function getManager()
    {
        return $this->doctrine->getManager();
    }
}
