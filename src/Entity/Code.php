<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CodeRepository")
 * @ORM\Table(indexes={
 *      @ORM\Index( name="code_index", columns={ "id" } )
 * })
 */
class Code
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=8)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $usedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="codes")
     */
    private $usedBy;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GameSession", mappedBy="code", cascade={"persist", "remove"})
     */
    private $gameSession;

    public function __construct( string $id )
    {
        $this->setId( $id );
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId( string $id ): self
    {
        $this->id = $id;
        
        return $this;
    }

    public function getUsedAt(): ?\DateTimeInterface
    {
        return $this->usedAt;
    }

    public function setUsedAt(?\DateTimeInterface $usedAt): self
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    public function getUsedBy(): ?User
    {
        return $this->usedBy;
    }

    public function setUsedBy(?User $usedBy): self
    {
        $this->usedBy = $usedBy;

        return $this;
    }

    public function getGameSession(): ?GameSession
    {
        return $this->gameSession;
    }

    public function setGameSession(GameSession $gameSession): self
    {
        $this->gameSession = $gameSession;

        // set the owning side of the relation if necessary
        if ($this !== $gameSession->getCode()) {
            $gameSession->setCode($this);
        }

        return $this;
    }
}
