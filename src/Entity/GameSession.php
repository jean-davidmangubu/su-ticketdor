<?php

namespace App\Entity;

use App\Validator\UserUnblocked;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\GameSessionStart;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameSessionRepository")
 * @ApiResource(
 *      normalizationContext={"groups"={"read_game_session"}},
 *      denormalizationContext={"groups"={"write_game_session"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"list_game_sessions"}}
 *          },
 *          "post"={
 *              "validation_groups"={"create_game_session"}
 *          },
 *          "start"={
 *              "method"="PUT",
 *              "path"="game_sessions/start",
 *              "controller"=GameSessionStart::class
 *          }
 *      },
 *      itemOperations={"get"}
 * )
 * @UserUnblocked(groups={"create_game_session"})
 */
class GameSession
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"preloaded_state", "read_game_session"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="gameSessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Code", inversedBy="gameSession", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(groups={"create_game_session"})
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GameSessionAttempt", mappedBy="gameSession", orphanRemoval=true)
     */
    private $attempts;
    
    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({ "read_game_session", "preloaded_state" })
     */
    private $state;
    
    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Groups({ "read_game_session", "preloaded_state" })
     */
    private $deadline;

    /**
     * @Groups({ "write_game_session" })
     */
    public $codeId;

    private $gameSessionAttemptLimit;

    public function __construct()
    {
        $this->attempts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlayer(): ?User
    {
        return $this->player;
    }

    public function setPlayer(?User $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getCode(): ?Code
    {
        return $this->code;
    }

    public function setCode(Code $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|GameSessionAttempt[]
     */
    public function getAttempts(): Collection
    {
        return $this->attempts;
    }

    public function addAttempt(GameSessionAttempt $attempt): self
    {
        if (!$this->attempts->contains($attempt)) {
            $this->attempts[] = $attempt;
            $attempt->setGameSession($this);
        }

        return $this;
    }

    public function removeAttempt(GameSessionAttempt $attempt): self
    {
        if ($this->attempts->contains($attempt)) {
            $this->attempts->removeElement($attempt);
            // set the owning side to null (unless already changed)
            if ($attempt->getGameSession() === $this) {
                $attempt->setGameSession(null);
            }
        }

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * @Groups({ "read_game_session" })
     */
    public function getAttemptCount(): int
    {
        return $this->attempts->count();
    }

    public function getGameSessionAttemptLimit(): ?string
    {
        return $this->gameSessionAttemptLimit;
    }

    public function setGameSessionAttemptLimit(string $gameSessionAttemptLimit): self
    {
        $this->gameSessionAttemptLimit = $gameSessionAttemptLimit;

        return $this;
    }

    /**
     * @Groups({ "read_game_session", "preloaded_state" })
     */
    public function getRemainingAttemptCount()
    {
        return $this->getGameSessionAttemptLimit() - $this->getAttempts()->count();
    }
    
    /**
     * @Groups({ "read_game_session", "preloaded_state" })
     */
    public function getTotalGainsAmount()
    {
        return array_reduce( $this->attempts->toArray(), function( $acc, $attempt ) {
            return $acc + ( $attempt->getInstantWin() ? $attempt->getInstantWin()->getRewardAmount() : 0 );
        }, 0 );
    }
}
