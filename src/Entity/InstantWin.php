<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstantWinRepository")
 * @ApiResource(
 *      itemOperations={"get"},
 *      collectionOperations={"get"}
 * )
 */
class InstantWin
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\Column(type="float")
     * @Groups({ "preloaded_state", "read_game_session_attempt" })
     */
    private $rewardAmount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "preloaded_state", "read_game_session_attempt" })
     */
    private $wonAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="instantWins")
     */
    private $wonBy;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\GameSessionAttempt", mappedBy="instantWin", cascade={"persist", "remove"})
     */
    private $gameSessionAttempt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getRewardAmount(): ?float
    {
        return $this->rewardAmount;
    }

    public function setRewardAmount(float $rewardAmount): self
    {
        $this->rewardAmount = $rewardAmount;

        return $this;
    }

    public function getWonAt(): ?\DateTimeInterface
    {
        return $this->wonAt;
    }

    public function setWonAt(?\DateTimeInterface $wonAt): self
    {
        $this->wonAt = $wonAt;

        return $this;
    }

    public function getWonBy(): ?User
    {
        return $this->wonBy;
    }

    public function setWonBy(?User $wonBy): self
    {
        $this->wonBy = $wonBy;

        return $this;
    }

    public function getGameSessionAttempt(): ?GameSessionAttempt
    {
        return $this->gameSessionAttempt;
    }

    public function setGameSessionAttempt(?GameSessionAttempt $gameSessionAttempt): self
    {
        $this->gameSessionAttempt = $gameSessionAttempt;

        // set (or unset) the owning side of the relation if necessary
        $newInstantWin = $gameSessionAttempt === null ? null : $this;
        if ($newInstantWin !== $gameSessionAttempt->getInstantWin()) {
            $gameSessionAttempt->setInstantWin($newInstantWin);
        }

        return $this;
    }
}
