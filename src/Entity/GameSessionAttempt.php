<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model\Timestampable\Timestampable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameSessionAttemptRepository")
 * @ApiResource(
 *      normalizationContext={"groups"={"read_game_session_attempt"}},
 *      denormalizationContext={"groups"={"write_game_session_attempt"}},
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"list_game_session_attempts"}}
 *          },
 *          "post"
 *      },
 *      itemOperations={"get"}
 * )
 * )
 */
class GameSessionAttempt
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups( "read_game_session_attempt" )
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GameSession", inversedBy="attempts", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $gameSession;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\InstantWin", inversedBy="gameSessionAttempt", cascade={"persist", "remove"})
     * @Groups( "read_game_session_attempt" )
     */
    private $instantWin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameSession(): ?GameSession
    {
        return $this->gameSession;
    }

    public function setGameSession(?GameSession $gameSession): self
    {
        $this->gameSession = $gameSession;

        return $this;
    }

    public function getInstantWin(): ?InstantWin
    {
        return $this->instantWin;
    }

    public function setInstantWin(?InstantWin $instantWin): self
    {
        $this->instantWin = $instantWin;

        return $this;
    }
}
