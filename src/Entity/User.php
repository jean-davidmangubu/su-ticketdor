<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use \Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({ "preloaded_state" })
     */
    private $email;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({ "preloaded_state" })
     */
    private $firstname;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({ "preloaded_state" })
     */
    private $lastname;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "preloaded_state" })
     */
    private $birthdate;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $card;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "preloaded_state" })
     */
    private $blockedUntil;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Code", mappedBy="usedBy")
     */
    private $codes;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InstantWin", mappedBy="wonBy")
     */
    private $instantWins;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GameSession", mappedBy="player", orphanRemoval=true)
     */
    private $gameSessions;
    
    /**
     * @ORM\Column(type="integer")
     * @Groups({ "preloaded_state" })
     */
    private $failedCodeAttemptCount;
    
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Groups({ "preloaded_state" })
     */
    private $title;

    public function __construct()
    {
        $this->codes = new ArrayCollection();
        $this->instantWins = new ArrayCollection();
        $this->gameSessions = new ArrayCollection();
        $this->failedCodeAttemptCount = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getCard(): ?string
    {
        return $this->card;
    }

    public function setCard(?string $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getBlockedUntil(): ?\DateTimeInterface
    {
        return $this->blockedUntil;
    }

    public function setBlockedUntil(?\DateTimeInterface $blockedUntil): self
    {
        $this->blockedUntil = $blockedUntil;

        return $this;
    }

    /**
     * @return Collection|Code[]
     */
    public function getCodes(): Collection
    {
        return $this->codes;
    }

    public function addCode(Code $code): self
    {
        if (!$this->codes->contains($code)) {
            $this->codes[] = $code;
            $code->setUsedBy($this);
        }

        return $this;
    }

    public function removeCode(Code $code): self
    {
        if ($this->codes->contains($code)) {
            $this->codes->removeElement($code);
            // set the owning side to null (unless already changed)
            if ($code->getUsedBy() === $this) {
                $code->setUsedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InstantWin[]
     */
    public function getInstantWins(): Collection
    {
        return $this->instantWins;
    }

    public function addInstantWin(InstantWin $instantWin): self
    {
        if (!$this->instantWins->contains($instantWin)) {
            $this->instantWins[] = $instantWin;
            $instantWin->setWonBy($this);
        }

        return $this;
    }

    public function removeInstantWin(InstantWin $instantWin): self
    {
        if ($this->instantWins->contains($instantWin)) {
            $this->instantWins->removeElement($instantWin);
            // set the owning side to null (unless already changed)
            if ($instantWin->getWonBy() === $this) {
                $instantWin->setWonBy(null);
            }
        }

        return $this;
    }

    // Implémentation UserInterface

    public function getUsername()
    {
        return $this->email;
    }

    public function getRoles()
    {
        return array( 'ROLE_USER' );
    }

    public function getPassword()
    {

    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|GameSession[]
     */
    public function getGameSessions(): Collection
    {
        return $this->gameSessions;
    }


    public function getGameLaunchedSession()
    {
        $sessions = [];
        $sessions = array_filter($this->gameSessions->toArray(), function($gameSession){
            if (  $gameSession->getState() !== 'new' )
            {
                return true;
            }
            return false;
        });

        return $sessions;
    }
    /**
     * @Groups({"preloaded_state"})
     */
    public function getGameLaunchedSessionCount()
    {
        return count( $this->getGameLaunchedSession() );
    }
    public function addGameSession(GameSession $gameSession): self
    {
        if (!$this->gameSessions->contains($gameSession)) {
            $this->gameSessions[] = $gameSession;
            $gameSession->setPlayer($this);
        }

        return $this;
    }

    public function removeGameSession(GameSession $gameSession): self
    {
        if ($this->gameSessions->contains($gameSession)) {
            $this->gameSessions->removeElement($gameSession);
            // set the owning side to null (unless already changed)
            if ($gameSession->getPlayer() === $this) {
                $gameSession->setPlayer(null);
            }
        }

        return $this;
    }

    /**
     * @Groups({"preloaded_state"})
     */
    public function getOpenGameSession()
    {
        $now = new \DateTime();
        foreach ( $this->gameSessions->toArray() as $gameSession )
        {
            if ( $gameSession->getDeadline() && $gameSession->getDeadline() > $now && $gameSession->getState() !== 'closed' )
            {
                return $gameSession;
            }
        }
        return NULL;
    }

    public function getAvailableGameSession()
    {
        $gs = $this->getAvailableGameSessions();
        return empty( $gs ) ? NULL : $gs[ 0 ];
    }
    
    /**
     * @Groups({"preloaded_state"})
     */
    public function getAvailableGameSessionCount()
    {
        return count( $this->getAvailableGameSessions() );
    }

    public function getAvailableGameSessions()
    {
        $now = new \DateTime();
        return array_values( array_filter( $this->gameSessions->toArray(), function( $gameSession ) use( $now ) {
            return ( !$gameSession->getDeadline() && $gameSession->getState() !== 'closed' );
        } ) );
    }

    /**
     * @Groups({"preloaded_state"})
     */
    public function getClosedGameSessionsCount()
    {
        return count( $this->getClosedGameSessions() );
    }

    public function getClosedGameSessions()
    {
        $now = new \DateTime();
        return array_values( array_filter( $this->gameSessions->toArray(), function( $gameSession ) use( $now ) {
            return ( ( $gameSession->getDeadline() && $gameSession->getDeadline() <= $now ) || $gameSession->getState() === 'closed' );
        } ) );
    }

    /**
     * @Groups({"preloaded_state"})
     */
    public function getTotalGainsAmount()
    {
        return $this->getTotalGainsAmountInPeriod();
    }

    public function getFailedCodeAttemptCount(): ?int
    {
        return $this->failedCodeAttemptCount;
    }

    public function setFailedCodeAttemptCount(int $failedCodeAttemptCount): self
    {
        $this->failedCodeAttemptCount = $failedCodeAttemptCount;

        return $this;
    }

    public function getInstantWinsInPeriod( $startDate = null, $endDate = null )
    {
        return array_filter( $this->instantWins->toArray(), function( $instantWin ) use( $startDate, $endDate ) {
            if ( ( $startDate && $instantWin->getWonAt() < $startDate ) || ( $endDate && $instantWin->getWonAt() > $endDate ) )
            {
                return false;
            }
            return true;
        } );
    }


    public function getTotalGainsAmountInPeriod( $startDate = null, $endDate = null )
    {
        return array_reduce( $this->getInstantWinsInPeriod( $startDate, $endDate ), function( $acc, $instantWin ) {
            return $acc + $instantWin->getRewardAmount();
        }, 0 );
    }

    public function getCurrentWeekInstantWins()
    {
        return $this->getInstantWinsInPeriod( new \DateTime( 'last Monday' ), new \DateTime() );
    }
    
    public function getCurrentDayInstantWins()
    {
        return $this->getInstantWinsInPeriod( new \DateTime( 'today' ), new \DateTime() );
    }

    public function getCurrentWeekGainsAmount()
    {
        return $this->getTotalGainsAmountInPeriod( new \DateTime( 'last Monday' ), new \DateTime() );
    }
    
    public function getCurrentDayGainsAmount()
    {
        return $this->getTotalGainsAmountInPeriod( new \DateTime( 'today' ), new \DateTime() );
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
