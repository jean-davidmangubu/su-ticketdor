<?php
namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;
use App\Security\CurrentUserProvider;

class UserUnblockedValidator extends ConstraintValidator
{
    private $currentUserProvider;

	public function __construct( CurrentUserProvider $currentUserProvider )
	{
		$this->currentUserProvider = $currentUserProvider;
	}

	public function validate( $value, Constraint $constraint = null )
	{
        $user = $this->currentUserProvider->getUser();
        $now = new \DateTime();
        if ( $user->getBlockedUntil() && $now < $user->getBlockedUntil() )
        {
            $this
                ->context
                ->buildViolation( $user->getBlockedUntil()->format( 'c' ) )
                ->atPath( 'blockedUntil' )
                ->addViolation()
            ;
            return false;
        }

		return true;
	}
}
