<?php
namespace App\Validator;

use App\Validator\UserUnblockedValidator;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserUnblocked extends Constraint
{
	public $message;

	public function validatedBy()
	{
		return UserUnblockedValidator::class;
	}

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}
