<?php

namespace App\EventSubscriber;

use App\Entity\GameSession;
use App\Service\MailManager;
use App\Repository\CodeRepository;
use App\Security\CurrentUserProvider;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\GameSessionRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

final class GameSessionEventSubscriber implements EventSubscriberInterface
{
    private $coreRepository;
    private $currentUserProvider;
    private $em;
    private $parameters;
    private $mailManager;

    public function __construct(
        CodeRepository $codeRepository,
        CurrentUserProvider $currentUserProvider,
        EntityManagerInterface $em,
        ParameterBagInterface $parameters,
        MailManager $mailManager
    )
    {
        $this->codeRepository = $codeRepository;
        $this->currentUserProvider = $currentUserProvider;
        $this->em = $em;
        $this->parameters = $parameters;
        $this->mailManager = $mailManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [ 'attachCode', EventPriorities::PRE_VALIDATE ],
        ];
    }

    public function attachCode( $event )
    {
        $gameSession = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ( !$gameSession instanceof GameSession || Request::METHOD_POST !== $method )
        {
            return;
        }
        
        $code = $this->codeRepository->findAvailableCodeById( $gameSession->codeId );
        $user = $this->currentUserProvider->getUser();
        if ( $code && $code->getUsedAt() === null )
        {
            $code->setUsedAt( new \DateTime() );
            $code->setUsedBy( $user );
            $gameSession->setCode( $code );
            $gameSession->setPlayer( $user );
            $gameSession->setState( 'new' );
            $user->setFailedCodeAttemptCount( 0 );
            //$this->mailManager->sendGameSessionCreationConfirmation( $user );
        }
        else
        {
            $user->setFailedCodeAttemptCount( $user->getFailedCodeAttemptCount() + 1 );
            $now = new \DateTimeImmutable();
            if ( 
                $user->getFailedCodeAttemptCount() >= $this->parameters->get( 'app.max_user_failed_code_attempts' )
                && ( !$user->getBlockedUntil() || $now > $user->getBlockedUntil() )
            )
            {
                $user->setFailedCodeAttemptCount( 0 );
                $user->setBlockedUntil( $now->add( new \DateInterval( 'P1D' ) ) );
            }
        }
        $this->em->persist( $user );
        $this->em->flush();
        if(!$code){
            throw new NotFoundHttpException(sprintf('The code "%s" does not exist.', $gameSession->codeId));
        }
    }
}