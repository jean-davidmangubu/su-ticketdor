<?php

namespace App\EventSubscriber;

use App\Entity\GameSessionAttempt;
use App\Repository\GameSessionAttemptRepository;
use App\Security\CurrentUserProvider;
use App\Repository\InstantWinRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\MailManager;

final class GameSessionAttemptEventSubscriber implements EventSubscriberInterface
{
    private $currentUserProvider;
    private $instantWinRepository;
    private $gameSessionAttemptRepository;
    private $parameters;
    private $mailManager;

    public function __construct(
        CurrentUserProvider $currentUserProvider,
        InstantWinRepository $instantWinRepository,
        GameSessionAttemptRepository $gameSessionAttemptRepository,
        ParameterBagInterface $parameters,
        MailManager $mailManager
    )
    {
        $this->currentUserProvider = $currentUserProvider;
        $this->instantWinRepository = $instantWinRepository;
        $this->gameSessionAttemptRepository = $gameSessionAttemptRepository;
        $this->parameters = $parameters;
        $this->mailManager = $mailManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                [ 'attachGameSessionAndInstantWin', EventPriorities::PRE_VALIDATE ],
                [ 'sendSuspectGainsAmountEmail', EventPriorities::POST_WRITE ],
                [ 'sendSuccessEmail', EventPriorities::POST_WRITE ],
            ]
        ];
    }

    public function attachGameSessionAndInstantWin( $event )
    {
        $gameSessionAttempt = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->currentUserProvider->getUser();

        $gameSession = $user->getOpenGameSession();
        if ( !$gameSessionAttempt instanceof GameSessionAttempt || Request::METHOD_POST !== $method || !$gameSession )
        {
            return;
        }

        $alreadywin = $this->gameSessionAttemptRepository->hasWin( $gameSession );

        // Si on dépasse le nombre de tentative autorisé par session
        if ( $gameSession && $gameSession->getAttempts()->count() >= $this->parameters->get( 'app.game_session_attempts_limit' ) - 1 )
        {
            // On cloture la session
            $gameSession->setState( 'closed' );
        }

        // On associe la session de jeu à la tentative
        $gameSessionAttempt->setGameSession( $gameSession );

        $instantWin = null;
        if ( $user->getCurrentDayGainsAmount() <= $this->parameters->get( 'app.maximum_daily_gains_amount' ) )
        {
            // On recherche un instant gagnant pour le créneau actuel
            $instantWin = $this->instantWinRepository->findOneByDate( new \DateTime() );
        }

        // Si un instant gagnant a été trouvé
        if ( $instantWin && $alreadywin < 1)
        {
            // On l'associe au joueur courant
            $instantWin->setWonBy( $user );
            $instantWin->setWonAt( new \DateTime() );

            // On l'associe à la tentative
            $gameSessionAttempt->setInstantWin( $instantWin );
        }

    }

    public function sendSuspectGainsAmountEmail( $event )
    {
        $gameSessionAttempt = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ( !$gameSessionAttempt instanceof GameSessionAttempt || Request::METHOD_POST !== $method )
        {
            return;
        }

        $user =  $this->currentUserProvider->getUser( true );

        $currentWeekGainsAmount = $user->getCurrentWeekGainsAmount();
        if ( $gameSessionAttempt->getInstantWin() && $currentWeekGainsAmount > $this->parameters->get( 'app.suspect_weekly_gains_amount' ) )
        {
            $this->mailManager->sendSuspectWeeklyGainsAmountEmail( $user );
        }

        $currentDayGainsAmount = $user->getCurrentDayGainsAmount();
        if ( $gameSessionAttempt->getInstantWin() && $currentDayGainsAmount > $this->parameters->get( 'app.suspect_daily_gains_amount' ) )
        {
            $this->mailManager->sendSuspectDailyGainsAmountEmail( $user );
        }
    }

    public function sendSuccessEmail( $event )
    {
        $gameSessionAttempt = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ( !$gameSessionAttempt instanceof GameSessionAttempt || Request::METHOD_POST !== $method )
        {
            return;
        }

        $instantWin = $gameSessionAttempt->getInstantWin();
        if ( $instantWin )
        {
            $user =  $this->currentUserProvider->getUser( true );
            $this->mailManager->sendGameSessionAttemptSuccess( $user, $instantWin->getRewardAmount() );
        }
    }
}