<?php

namespace App\Controller;

use App\Security\CurrentUserProvider;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GameSessionStart {
    private $currentUserProvider;
    private $parameters;

    public function __construct( CurrentUserProvider $currentUserProvider, ParameterBagInterface $parameters )
    {
        $this->currentUserProvider = $currentUserProvider;
        $this->parameters = $parameters;
    }

    public function __invoke()
    {
        $user = $this->currentUserProvider->getUser();

        // On vérifie d'abord si une session de jeu n'est pas déjà ouverte
        $gameSession = $user->getOpenGameSession();
        if ( !$gameSession )
        {
            // Si aucune n'est ouverte, on regarde si une est disponible
            $gameSession = $user->getAvailableGameSession();
            if ( $gameSession )
            {
                // Si une session est dispo, on lui déifinit une date de fin, et on lui met le statut "open"
                $gameSession->setDeadline( new \DateTimeImmutable( '+' . $this->parameters->get( 'app.game_session_duration' ) . ' seconds' ) );
                $gameSession->setState( 'open' );
            }
            else
            {
                // Sinon on notifie qu'on a trouvé aucune session de jeu
                throw new NotFoundHttpException( 'game_session.not_found' );                
            }
        }

        // On retourne la session de jeu en cours ou une nouvelle session si disponible
        return $gameSession;
    }
}