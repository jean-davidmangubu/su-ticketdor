<?php

namespace App\Controller;

use App\Entity\Code;
use App\Entity\GameSession;
use App\Entity\User;
use App\Entity\InstantWin;
use App\Validator\UserUnblockedValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use \Sinergi\BrowserDetector\Os;
use App\Service\MailManager;


class MainController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @Route("/game", name="game")
     * @Route("/game-tutorial", name="gameTurorial")
     * @Route("/faq", name="faq")
     * @Route("/video-refused", name="videoRefused")
     */
    public function index(Request $request)
    {
		$iwRepository = $this->getDoctrine()->getRepository( InstantWin::class );
        $user = $this->getUser();
        return $this->render( 'main/index.html.twig', [
            'preloadedState' => $this->get( 'serializer' )->serialize([
                'user' => $user,
                'rewardsState' => [
                    'total' => $iwRepository->calcTotalRewardsSum(),
                    'won' => $iwRepository->calcWonRewardsSum()
                ]
            ], 'json', [ 'groups' => [ 'preloaded_state' ] ] ),
            'maxUserFailedCodeAttempts' => $this->getParameter( 'app.max_user_failed_code_attempts' ),
            'gameSessionAttemptsLimit' => $this->getParameter( 'app.game_session_attempts_limit' ),
            'facebookAppId' => $this->getParameter( 'app.facebook_app_id' ),
            'ga_id' => $this->getParameter( 'app.ga_id' ),
            'loginUrl' => $this->getParameter( 'app.login_url' ),
            'redirectUrl' => $this->getParameter( 'app.redir_url' ),
			'og_title' => $this->getParameter('app.og_title'),
			'og_img' => $this->getParameter('app.og_img'),
			'og_description' => $this->getParameter('app.og_description'),
			'shareUrl' => $this->getParameter( 'app.share_url' )
        ] );
    }
    /**
     * @Route("/pre_home", name="prehome2")
     */
    public function prehome(Request $request)
    {
        //$iwRepository = $this->getDoctrine()->getRepository( InstantWin::class );
        $user = false;
        return $this->render( 'main/index.html.twig', [
            'preloadedState' => $this->get( 'serializer' )->serialize([
                'user' => $user,
                'rewardsState' => [
                    'total' => 0,
                    'won' => 0
                ]
            ], 'json', [ 'groups' => [ 'preloaded_state' ] ] ),
            'maxUserFailedCodeAttempts' => $this->getParameter( 'app.max_user_failed_code_attempts' ),
            'gameSessionAttemptsLimit' => $this->getParameter( 'app.game_session_attempts_limit' ),
            'facebookAppId' => $this->getParameter( 'app.facebook_app_id' ),
            'ga_id' => $this->getParameter( 'app.ga_id' ),
            'loginUrl' => $this->getParameter( 'app.login_url' ),
            'redirectUrl' => $this->getParameter( 'app.redir_url' ),
            'og_title' => $this->getParameter('app.og_title'),
            'og_img' => $this->getParameter('app.og_img'),
            'og_description' => $this->getParameter('app.og_description'),
            'shareUrl' => $this->getParameter( 'app.share_url' )
        ] );
    }

    /**
     * @Route("/relance-code", name="relance-code")
     */
    public function relanceCode(Request $request)
    {

        $prenom = $request->query->get('prenom');

        return $this->render('mail/relance-code-html.twig',
            ['prenom' => $prenom]);

    }
    /**
     * @Route("/test-mail-gain", name="test-mail-gain")
     */
    public function testMailAttempt(){
        $user =  $this->getUser( true );
        $instantWin = $this->getDoctrine()->getRepository(InstantWin::class)->findOneBy(
            ['wonBy' => $user]
        );

        dump($user);
        dump($instantWin);
        $out = $this->get('App\Service\MailManager')->sendGameSessionAttemptSuccess( $user, $instantWin->getRewardAmount() );
        dump($out);

        $rewardAmount = $instantWin->getRewardAmount();
        $totalReward = $user->getTotalGainsAmount();
        $user = [
            'firstname' => $user->getFirstname()
        ];

        return $this->render('mail/game-session-attempt-success.html.twig',
            [
                'user' => $user,
                'app_url' => 'https://www.magasins-u.com/leticketdor',
                'faq_url' =>  $this->getParameter( 'app.faq_url' ),
                'reward_amount' => $rewardAmount,
                'total_reward' => $totalReward
            ]);

    }
    /**
     * @Route("/mail-gain", name="mail-gain")
     */
    public function mailGain(Request $request)
    {

        $rewardAmount = $request->query->get('amount');
        $totalReward = $request->query->get('total_reward');
        $user = [
            'firstname' => $request->query->get('prenom')
        ];

        return $this->render('mail/game-session-attempt-success.html.twig',
            [
                'user' => $user,
                'app_url' => 'https://www.magasins-u.com/leticketdor',
                'faq_url' =>  $this->getParameter( 'app.faq_url' ),
                'reward_amount' => $rewardAmount,
                'total_reward' => $totalReward
            ]);

    }

    public function iframeSimulator( $loginMode, Request $request )
    {
        if ( $this->container->has( 'profiler' ) )
        {
            $this->container->get( 'profiler' )->disable();
        }
        $userRepository = $this->getDoctrine()->getRepository( User::class );
        return $this->render( 'main/iframe-simulator.html.twig', [
            'appSecret' => $this->getParameter( 'kernel.secret' ),
            'users' => $userRepository->findAll(),
            'loginMode' => $loginMode,
            'user' => json_decode( $request->get( 'user', 'null' ) )
        ] );
    }

    public static function getSubscribedServices()
    {
        $out = parent::getSubscribedServices();
        $out['App\Service\MailManager'] = \App\Service\MailManager::class;
        return $out;
    }

}
