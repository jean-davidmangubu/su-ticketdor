<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserProvider implements UserProviderInterface
{
    private $em;

    public function __construct( EntityManagerInterface $em )
    {
        $this->em = $em;
    }

    public function loadUserByUsername( $data )
    {
        $userData = json_decode( base64_decode( $data ) );

        if(strlen($userData->naissanceDate) > 9){
            $userData->naissanceDate = substr($userData->naissanceDate, 0, 9);
        }


        $user =  $this->fetchUser( $userData->email );

        if ( !$user )
        {
            $user = new User();
            $user->setEmail( $userData->email );
        }

        $date = false;
        if($userData->naissanceDate){

            $date = \DateTime::createFromFormat( 'U', $userData->naissanceDate );
        }

        $user
            ->setFirstname( $userData->prenom )
            ->setLastname( $userData->nom )
            ->setCard( $userData->carteu )
            ->setTitle( $userData->civilite )
            ->setBirthdate( $userData->naissanceDate && $date ? $date : null );

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if ( !$user instanceof User )
        {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        $username = $user->getUsername();

        return $this->fetchUser( $username );
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }

    private function fetchUser( $email )
    {
        return $this->em->getRepository( User::class )->findOneByEmail( $email );
    }
}