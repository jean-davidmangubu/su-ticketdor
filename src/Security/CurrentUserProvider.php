<?php

namespace App\Security;

use App\Security\UserProvider;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CurrentUserProvider
{
    private $tokenStorage;
    private $userProvider;

    public function __construct(TokenStorageInterface $tokenStorage, UserProvider $userProvider )
    {
        $this->tokenStorage = $tokenStorage;
        $this->userProvider = $userProvider;
    }

    public function getUser( $refresh = false )
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            return;
        }

        if ( $user && $refresh )
        {
            $user = $this->userProvider->refreshUser( $user );
        }
        
        return $user;
    }
}
