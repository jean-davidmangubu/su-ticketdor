<?php
// src/Security/TokenAuthenticator.php
namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UserAuthenticator extends AbstractGuardAuthenticator
{
    private $em;
    private $parameters;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $parameters )
    {
        $this->em = $em;
        $this->parameters = $parameters;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return $request->query->has( 'user' ) && $request->query->has( 'hash' ) && trim($request->query->get( 'user' )) != "";
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        return array(
            'userData' => $request->query->get( 'user' ),
            'hash' => $request->query->get( 'hash' )
        );
    }

    public function getUser( $credentials, UserProviderInterface $userProvider )
    {
        return $userProvider->loadUserByUsername( $credentials['userData' ] );
    }

    public function checkCredentials( $credentials, UserInterface $user )
    {
		return true;
        return sha1( sha1( $credentials[ 'userData' ] . $this->parameters->get( 'kernel.secret' )  ) ) == $credentials[ 'hash' ];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $user = $token->getUser();
        $this->em->persist( $user );
        $this->em->flush();


        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        throw new AccessDeniedHttpException();
    }

    public function supportsRememberMe()
    {
        return false;
    }
}