<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180814135148 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE code (id VARCHAR(8) NOT NULL, used_by_id INT DEFAULT NULL, used_at DATETIME DEFAULT NULL, INDEX IDX_771530984C2B72A8 (used_by_id), INDEX code_index (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, birthdate DATETIME DEFAULT NULL, card VARCHAR(255) DEFAULT NULL, blocked_until DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instant_win (id INT AUTO_INCREMENT NOT NULL, won_by_id INT DEFAULT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, reward_amount DOUBLE PRECISION NOT NULL, won_at DATETIME DEFAULT NULL, INDEX IDX_A18CDF0C31945226 (won_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE code ADD CONSTRAINT FK_771530984C2B72A8 FOREIGN KEY (used_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE instant_win ADD CONSTRAINT FK_A18CDF0C31945226 FOREIGN KEY (won_by_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE code DROP FOREIGN KEY FK_771530984C2B72A8');
        $this->addSql('ALTER TABLE instant_win DROP FOREIGN KEY FK_A18CDF0C31945226');
        $this->addSql('DROP TABLE code');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE instant_win');
    }
}
