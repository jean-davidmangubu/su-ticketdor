<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180821094324 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game_session_attempt (id INT AUTO_INCREMENT NOT NULL, game_session_id INT NOT NULL, instant_win_id INT DEFAULT NULL, INDEX IDX_758447DB8FE32B32 (game_session_id), UNIQUE INDEX UNIQ_758447DB8DF52A2E (instant_win_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_session (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, code_id VARCHAR(8) NOT NULL, INDEX IDX_4586AAFB99E6F5DF (player_id), UNIQUE INDEX UNIQ_4586AAFB27DAFE17 (code_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_session_attempt ADD CONSTRAINT FK_758447DB8FE32B32 FOREIGN KEY (game_session_id) REFERENCES game_session (id)');
        $this->addSql('ALTER TABLE game_session_attempt ADD CONSTRAINT FK_758447DB8DF52A2E FOREIGN KEY (instant_win_id) REFERENCES instant_win (id)');
        $this->addSql('ALTER TABLE game_session ADD CONSTRAINT FK_4586AAFB99E6F5DF FOREIGN KEY (player_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE game_session ADD CONSTRAINT FK_4586AAFB27DAFE17 FOREIGN KEY (code_id) REFERENCES code (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE game_session_attempt DROP FOREIGN KEY FK_758447DB8FE32B32');
        $this->addSql('DROP TABLE game_session_attempt');
        $this->addSql('DROP TABLE game_session');
    }
}
