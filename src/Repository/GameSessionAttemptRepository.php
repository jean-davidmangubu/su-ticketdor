<?php

namespace App\Repository;

use App\Entity\GameSession;
use App\Entity\GameSessionAttempt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GameSessionAttempt|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameSessionAttempt|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameSessionAttempt[]    findAll()
 * @method GameSessionAttempt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameSessionAttemptRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameSessionAttempt::class);
    }

    public function hasWin( GameSession $gameSession ){
        return $this->createQueryBuilder('a')
            ->select('count(a)')
            ->andWhere('a.gameSession = :gs and a.instantWin is not null ')
            ->setParameter('gs', $gameSession->getId())
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
//    /**
//     * @return GameSessionAttempt[] Returns an array of GameSessionAttempt objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameSessionAttempt
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
