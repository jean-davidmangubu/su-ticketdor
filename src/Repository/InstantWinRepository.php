<?php

namespace App\Repository;

use App\Entity\InstantWin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InstantWin|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstantWin|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstantWin[]    findAll()
 * @method InstantWin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstantWinRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InstantWin::class);
    }

    public function calcWonRewardsSum(): int
    {
        return $this
            ->createQueryBuilder( 'iw' )
            ->select( 'SUM( iw.rewardAmount )' )
            ->where( 'iw.wonAt IS NOT NULL' )
            ->getQuery()
            ->getSingleScalarResult() ?? 0
        ;
    }

    public function calcTotalRewardsSum(): int
    {
        return $this
            ->createQueryBuilder( 'iw' )
            ->select( 'SUM( iw.rewardAmount )' )
            ->getQuery()
            ->getSingleScalarResult() ?? 0
        ;
    }

    public function findOneByDate( $date ): ?InstantWin
    {
        return $this
            ->createQueryBuilder( 'i' )
            ->andWhere( ':date >= i.startDate' )
            ->andWhere( ':date < i.endDate' )
            ->andWhere( 'i.wonAt IS NULL' )
            ->setParameter( 'date', $date->format( 'c' ) )
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

//    /**
//     * @return InstantWin[] Returns an array of InstantWin objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstantWin
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
