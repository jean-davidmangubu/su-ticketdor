<?php

namespace App\EventListener;

use App\Entity\Product;
use App\Entity\GameSession;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GameSessionEventListener
{
    private $parameters;

    public function __construct( ParameterBagInterface $parameters )
    {
        $this->parameters = $parameters;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $gameSession = $args->getEntity();

        if (!$gameSession instanceof GameSession) {
            return;
        }

        $gameSession->setGameSessionAttemptLimit( $this->parameters->get( 'app.game_session_attempts_limit' ) );
    }
}